from quart import Quart

import sentry_sdk, os
from sentry_sdk.integrations.quart import QuartIntegration

sentry_sdk.init(
    dsn=os.environ["QUART_SENTRY_DSN"],
    enable_tracing=True,
    integrations=[
        QuartIntegration(),
    ],
)

app = Quart(__name__)

@app.route("/")
async def hello():
    1/0  # raises an error
    return {"hello": "world"}

app.run()
